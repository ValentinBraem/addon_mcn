import bpy
import math

class MCN_PT_VB(bpy.types.Panel):
    """"""
    bl_label = "VB"
    bl_idname = "MCN_PT_VB"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = "Tool"
    bl_parent_id = "MCN_PT_Main_Panel"
    bl_options = {'DEFAULT_CLOSED'}

    def draw(self, context):
        layout = self.layout
        layout.label(text="VB LightKit")

        row = layout.row()
        row.operator("object.add_lightkit", icon='OUTLINER_OB_LIGHT')


class MCN_OT_VB(bpy.types.Operator):
    """Add a simple lightkit"""
    bl_idname = "object.add_lightkit"
    bl_label = "Add the LightKit"
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        #Set the render engine
        bpy.context.scene.render.engine = 'CYCLES'
        bpy.context.scene.cycles.device = 'GPU'
        
        #Create the backdrop
        verts = [(-2,1,0), (2,1,0), (2,-1,0), (-2,-1,0), (-2,1,2), (2,1,2)]
        faces = [(0,1,2,3), (0,4,5,1)]     
                   
        mesh = bpy.data.meshes.new("Backdrop")
        object = bpy.data.objects.new("Backdrop", mesh)
        
        bpy.context.collection.objects.link(object)
        
        mesh.from_pydata(verts,[],faces)
    
        modifier = object.modifiers.new(name='Bevel', type='BEVEL')
        modifier.width = 0.2
        modifier.segments = 4
    
        object.data.polygons.foreach_set('use_smooth',  [True] * len(object.data.polygons))
        object.data.update()
        
        #Create and set the camera
        camera_data = bpy.data.cameras.new("Camera")
        camera_object = bpy.data.objects.new("Camera", camera_data)
        
        camera_object.location = (0, -2.75, 0.9)
        camera_object.rotation_euler.x = math.radians(77)
        
        bpy.context.collection.objects.link(camera_object)
        
        #Create and set the lights
        toplight_data = bpy.data.lights.new(name="TopLight", type='AREA')
        toplight_object = bpy.data.objects.new(name="TopLight", object_data=toplight_data)
        
        leftlight_data = bpy.data.lights.new(name="LeftLight", type='AREA')
        leftlight_object = bpy.data.objects.new(name="LeftLight", object_data=leftlight_data)
        
        rightlight_data = bpy.data.lights.new(name="RightLight", type='AREA')
        rightlight_object = bpy.data.objects.new(name="RightLight", object_data=rightlight_data)
        
        toplight_object.data.size = 1
        toplight_object.location.z = 2
        
        leftlight_object.data.size = 1
        leftlight_object.location = (-2, 0, 1)
        leftlight_object.rotation_euler.y = math.radians(-70)
        
        rightlight_object.data.size = 1
        rightlight_object.location = (2, 0, 1)
        rightlight_object.rotation_euler.y = math.radians(70)
        
        bpy.context.collection.objects.link(toplight_object)
        bpy.context.collection.objects.link(leftlight_object)
        bpy.context.collection.objects.link(rightlight_object)
                            
        return {'FINISHED'}

def register():
    bpy.utils.register_class(MCN_PT_VB)
    bpy.utils.register_class(MCN_OT_VB)


def unregister():
    bpy.utils.unregister_class(MCN_PT_VB)
    bpy.utils.unregister_class(MCN_OT_VB)